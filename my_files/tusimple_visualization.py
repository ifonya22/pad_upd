import json
import ast
import cv2
import os
from progress.bar import IncrementalBar

class TusimpleVisualizator:

    def __init__(self, path, save_path):
        self.path = path
        self.save_path = save_path

    
    def get_points(self, ):
        arr_points = []
        raw_file = []

        with open(self.path, 'r') as f:
            ff = f.readline()
            while ff != '':
                data = ast.literal_eval(ff)
                ff = f.readline()

                arr = []
                for d in data['lanes']:
                    ap = []
                    for width, height in zip(d, data['h_samples']):
                        if width != -2:
                            ap.append([width, height])
                    arr.append(ap)

                arr_points.append(arr)

                dt = data['raw_file'].replace('\\', '')
                raw_file.append(dt)

            f.close()
        return arr_points, raw_file


    def add_points(self, arr_points, img_path):
        image = cv2.imread('/home/ilya/PycharmProjects/LSTR/TuSimple/LaneDetection/' + img_path)
        output = image.copy()

        w, h, _ = output.shape
        if w != 1280 or h != 720:
            output = cv2.resize(output, (1280, 720), interpolation = cv2.INTER_AREA)


        color = (0, 0, 255)
        for ap in arr_points:
            for idx in range(len(ap)-1):
                cv2.line(output, (int(ap[idx][0]), int(ap[idx][1])),
                (int(ap[idx+1][0]), int(ap[idx+1][1])), color, 5)
                
        # print(f'{self.save_path}/{img_path}')
        try:
            os.makedirs(f'{self.save_path}/{img_path.split("/")[0]}/{img_path.split("/")[1]}/{img_path.split("/")[2]}')
        except:
            pass
        
        cv2.imwrite(f'{self.save_path}/{img_path}', output)

        # cv2.imshow(img_path, output)
        # cv2.waitKey()
        # cv2.destroyAllWindows()




def go(iter_num, a):
    orig_imgs_path = f'/home/ilya/PycharmProjects/pytorch-auto-drive/output/{a}.json'
    predicted_imgs_path = f'/home/ilya/PycharmProjects/pytorch-auto-drive/output/imgs/{a}'

    tv = TusimpleVisualizator(orig_imgs_path, predicted_imgs_path)

    ap, ip = tv.get_points()
    bar = IncrementalBar(f'{iter_num}s iteration', max = len(ap))
    for i in range(len(ap)):
        tv.add_points(ap[i], ip[i])
        bar.next()
    bar.finish()

# if __name__ == '__main__':
#     # go('resnet101_baseline_tusimple')
#     # go('resnet101_scnn_tusimple')
#     # go('resnet101_resa_tusimple')
#     # go('resnet18s_lstr-aug_tusimple')

#     models_list = ['resnet101_baseline_tusimple', 'resnet101_scnn_tusimple', 
#                     'resnet101_resa_tusimple', 'resnet18s_lstr-aug_tusimple', 
#                     'resnet34_bezierlanenet_tusimple-aug2']

#     iter_num = 1
#     for model in models_list:
#         go(iter_num, f'{model}')
#         iter_num += 1

from cgi import print_form
import os
import random

def create_rand_test_txt(paths, num_of_folders, validation=True):
    txt_path = '/home/ilya/PycharmProjects/LSTR/TuSimple/LaneDetection/lists/test.txt' 
    # f = open(txt_path, 'a')
    # if not f:
    #     f = open (txt_path, "w")

    f = open (txt_path, "w")
    cnt = 0
    if validation:
        while (cnt < num_of_folders):
            rand_num = random.randint(0, len(paths) - 1)
            f.write(f'0531/{paths[rand_num]}/20\n')
            cnt += 1
    else:
        while (cnt < num_of_folders):
            rand_num = random.randint(0, len(paths) - 1)
            for num in range(1, 21):
                f.write(f'0531/{paths[rand_num]}/{num}\n')
            cnt += 1
    f.close()

def create_text_txt(path):
    txt_path = '/home/ilya/PycharmProjects/LSTR/TuSimple/LaneDetection/lists/test.txt' 
    f = open (txt_path, "w")

    imgs = os.listdir(path)

    for i in imgs:
        f.write(f'0530/test2/{i.split(".")[0]}\n')

def make_txt():
    # path = '/home/ilya/PycharmProjects/LSTR/TuSimple/LaneDetection/clips/0530/test2'
    # create_text_txt(path)
    path = '/home/ilya/PycharmProjects/LSTR/TuSimple/LaneDetection/clips/0531'
    paths = os.listdir(path)
    create_rand_test_txt(paths, 100, validation=True)

if __name__ == '__main__':
    make_txt()
    # path = '/home/ilya/PycharmProjects/LSTR/TuSimple/LaneDetection/clips/0531'
    # paths = os.listdir(path)

    
    # create_rand_test_txt(paths, 100, validation=True)
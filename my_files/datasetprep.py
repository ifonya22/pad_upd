import os
import json
import cv2


class DatasetPreparation:
    def __init__(self):
        self.curdir = os.path.abspath(os.curdir)

    def video_cutting(self, videoname, n=100):
        print(f'{videoname} cutting...')
        videocapture = cv2.VideoCapture(f'{self.curdir}/../Videos/{videoname}')
        success, image = videocapture.read()
        cnt = 0
        os.makedirs(f'{self.curdir}/../LaneDetection/clips/0530/{videoname.split(".")[0]}')
        while success:
            if cnt % n == 0:
                cv2.imwrite(f'{self.curdir}/../LaneDetection/clips/0530/{videoname.split(".")[0]}/{cnt}.jpeg',
                            image)
                print(f'{cnt}.jpeg done')
            success, image = videocapture.read()
            cnt += 1
        print('cutting complete')

    def photos_info_to_json(self, images_path='', json_path=''):
        print('adding photos to json...')
        if images_path == '':
            images_path = f'{self.curdir}/../LaneDetection/clips/0530'
        if json_path == '':
            json_path = f'{self.curdir}/../LaneDetection/test_label.json'

        with open(json_path, "w") as write_file:
            for vn in os.listdir(f'{self.curdir}/../Videos'):
                vn = vn.split('.')[0]
                print(vn)
                for img_name in os.listdir(f'{images_path}/{vn}'):
                    data = {"lanes": [[-2 for _ in range(56)],
                                      [-2 for _ in range(56)]],
                            "h_samples": [160 + 10 * i for i in range(56)],
                            "raw_file": f"clips/0530/{vn}/{img_name}"
                            }
                    json.dump(data, write_file)
                    write_file.write('\n')
        try:
            os.remove(f"{self.curdir}/cache/tusimple_['test_label'].pkl")
        except:
            print("tusimple_['test_label'].pkl already deleted")
        print('adding complete')

    def videos_names(self):
        print(self.curdir)
        return os.listdir(f'{self.curdir}/../Videos')


if __name__ == '__main__':
    ds = DatasetPreparation()
    # for vn in ds.videos_names():
    #     ds.video_cutting(vn)
    ds.photos_info_to_json()

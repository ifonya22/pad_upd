# compare data['lanes'](prediction) and dataset['lanes'](validation) 

import ast
from audioop import cross
from sklearn import metrics
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score
import pandas as pd

class TusimpleMetrics:

    def __init__(self, prediction_path, validation_path):
        self.prediction_path = prediction_path
        self.validation_path = validation_path
        # self.save_path = save_path


    def get_data(self, path):
        data_list = []

        with open(path, 'r') as f:
            read_line = f.readline()
            while read_line != '':
                data = ast.literal_eval(read_line)
                data['raw_file'] = data['raw_file'].replace('\\', '')
                data_list.append(data) 
                read_line = f.readline()
        
        return data_list


    def get_mse(self, a, b):
        # return mean_squared_error(a, b, squared=False)
        mse = []
        for i in range(len(a)):
            if abs(a[i] - b[i]) > 100:
                mse.append(10)
            else:
                mse.append((a[i] - b[i])**2)
        return (sum(mse)/len(mse))


    def get_accuracy(self, list1, list2, tol=20):
        tp = 0
        tn = 0
        fp = 0
        fn = 0
      
        # 610 602
        # 610 500
        # -2 160
        # 500 -2
        # -2 -2 
        for i in range(len(list1)):
            if list2[i] == -2 and list1[i] == -2:
                tn += 1
            elif abs(list1[i] - list2[i]) <= tol:
                tp += 1
            elif list2[i] == -2 and list1[i] != -2:
                fp += 1
            else:
                fn += 1

        return tp, tn, fp, fn

    def get_metrics_arr(self, list1, list2):
        metrics_arr = []
        tptnfpfn = [0]*4
        for i in range(len(list1)):
            rmse = self.get_mse(list1[i], list2[i])
            tp, tn, fp, fn = self.get_accuracy(list1[i], list2[i])
            tptnfpfn[0] += tp
            tptnfpfn[1] += tn
            tptnfpfn[2] += fp
            tptnfpfn[3] += fn
            metrics_arr.append(rmse)
        return metrics_arr, tptnfpfn

    def get_mean(self, a):
        if len(a) != 0:
            return sum(a) / len(a)
        else:
            return 0

    def compare(self):
        arr_pred = self.get_data(self.prediction_path)
        val_pred = self.get_data(self.validation_path)
        arr_mean = []

        for d1 in arr_pred:
            for d2 in val_pred:
                if d1['raw_file'] == d2['raw_file']:
                    if len(d1['lanes']) == len(d2['lanes']):
                        metrics_arr, tptnfpfn = self.get_metrics_arr(d1['lanes'], d2['lanes'])
                        
                        arr_mean.append([d1['raw_file'], self.get_mean(metrics_arr), *tptnfpfn])

        return arr_mean

def go(a):
    prediction_path = f'/home/ilya/PycharmProjects/pytorch-auto-drive/output/{a}.json'
    validation_path = f'/home/ilya/PycharmProjects/pytorch-auto-drive/output/1.json'
    v = f'/home/ilya/PycharmProjects/LSTR/TuSimple/LaneDetection/test_label.json'
    tm = TusimpleMetrics(prediction_path, v)
    m = tm.compare()
    # print(m)
    pd.DataFrame(m, columns = ['path', 'mse', 'tp', 'tn', 'fp', 'fn']).to_csv(f'/home/ilya/PycharmProjects/pytorch-auto-drive/my_files/{a}.csv')
    print(f'{a} mean rmse : {tm.get_mean([i[1] for i in m])}')
    
if __name__ == '__main__':
    models_list = ['resnet101_baseline_tusimple', 'resnet101_scnn_tusimple', 'resnet101_resa_tusimple', 'resnet18s_lstr-aug_tusimple', 'resnet34_bezierlanenet_tusimple-aug2']
    for model in models_list:
        go(f'{model}')
import os
from datetime import datetime


def visualise(models_list):
    iter_num = 1
    for model in models_list:
        go(iter_num, f'{model}')
        iter_num += 1


def go(iter_num, a):
    orig_imgs_path = f'/home/ilya/PycharmProjects/pytorch-auto-drive/output/{a}.json'
    predicted_imgs_path = f'/home/ilya/PycharmProjects/pytorch-auto-drive/output/imgs/{a}'

    tv = TusimpleVisualizator(orig_imgs_path, predicted_imgs_path)

    ap, ip = tv.get_points()
    bar = IncrementalBar(f'{iter_num}s iteration', max = len(ap))
    for i in range(len(ap)):
        tv.add_points(ap[i], ip[i])
        bar.next()
    bar.finish()


if __name__ == '__main__':
    models_list = ['resnet101_baseline_tusimple', 'resnet101_scnn_tusimple',
                    'resnet101_resa_tusimple', 'resnet18s_lstr-aug_tusimple',
                    'resnet34_bezierlanenet_tusimple-aug2']
    # methods = os.popen('cd /home/ilya/PycharmProjects/pytorch-auto-drive; cd configs/lane_detection/; ls').read()
    configs_list = ['baseline/resnet101_tusimple', 'scnn/resnet101_tusimple',
                    'resa/resnet101_tusimple', 'lstr/resnet18s_tusimple_aug',
                    'bezierlanenet/resnet34_tusimple_aug1b']
  
    for config in configs_list:
        start_time = datetime.now()
        os.system(f'cd /home/ilya/PycharmProjects/pytorch-auto-drive;  python main_landet.py --test --config=configs/lane_detection/{config}.py')
        run_time = datetime.now() - start_time
        print(f'run time is {run_time}')

    from tusimple_visualization import TusimpleVisualizator, IncrementalBar
    visualise(models_list)
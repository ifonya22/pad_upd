import cv2
import os

def go(path):
    ls = os.listdir(path)
    num = 10
    for file in ls:
        if file.split('.')[1] != 'sjpg':
            
            img = cv2.imread(f'{path}/{file}')
            cv2.imwrite(f'{path}/{num}.jpg', img) 
            print(f'{file}->{num}.jpg')
            num += 1

if __name__ == '__main__':
    go('/home/ilya/PycharmProjects/LSTR/TuSimple/LaneDetection/clips/0530/test')